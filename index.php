<!doctype html>
<html>
<head>
    <title>Shady Notes</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>

<body class="mainbody">
<div>
	<div class="topnav">
		<a href="index.php" id="homelink"><img src="/assets/navlogo.png" class="navimg"/></a>
		<a href="signin.php" style="float:right">Sign-In</a>
		<a href="demo.php">Demo</a>
	</div>

	<div class="maincontent">
		<div class="maincontentcolumn">
			<div class="center">
			<img style="width:61%;" src="/assets/navlogo.png">
			<p> A safe, no-nonensense site to store all your notes. Complete with a WYSIWYG markdown editor. 
			Nothing shady about what's going on here.
			</p>
			</div>
		</div>
	</div>
	<div>
		<a href="demo.php" class="button">Demo</a>
	</div>
</div>
</body>
</html>

