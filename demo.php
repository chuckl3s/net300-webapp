<!doctype html>
<html>
<head>
    <title>Shady Notes</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" href="/css/editor.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
	<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>
</head>

<body>
<div>
	<div class="topbar">
		<a href="index.php" id="homelink"><img src="/assets/navlogo.png" class="navimg"/></a>
		<div class="top-search">
			<form method="POST">
				<input type="text" placeholder="Search..." name="search">
				<button type="submit"><i class="fa fa-search"></i></button>
		<a href="signin.php" style="float:right">Sign-In</a>
		<a href="demo.php">Demo</a>
	</div>

	<div class="maincontent">
		<div class="maincontentcolumn">
			<div class="center">
	<textarea id="note_editor" class="center">

# Intro
Go ahead, play around with the editor! Be sure to check out **bold** and *italic* styling, or even [links](https://google.com). You can type the Markdown syntax, use the toolbar, or use shortcuts like `cmd-b` or `ctrl-b`.

## Lists
Unordered lists can be started using the toolbar or by typing `* `, `- `, or `+ `. Ordered lists can be started by typing `1. `.

#### Unordered
* Lists are a piece of cake
* They even auto continue as you type
* A double enter will end them
* Tabs and shift-tabs work too

#### Ordered
1. Numbered lists...
2. ...work too!

## What about images?
![Yes](https://i.imgur.com/sZlktY7.png)

	</textarea>
	</div>
	</div>
	</div>

<!--
<script>
var simplemde = new SimpleMDE({ element: document.getElementById("note_editor") });
</script>
-->

</div>
</body>
</html>

